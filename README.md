# Tempedia

Tempedia is unofficial documentation on [Temtem](https://crema.gg/games/temtem/) species, techniques, traits, types, conditions, and gear.  It is styled to look like the actual interface you see when you play the game.

## Licensing

All HTML and JSON is licensed under [Creative Commons Attribution-Non-Commercial-ShareAlike 3.0 License](https://creativecommons.org/licenses/by-nc-sa/3.0/) because it originates from the [Official Temtem Wiki](https://temtem.wiki.gg/).  Media elements are copyright of [Crema Games](https://crema.gg/), but used under assumption of [Fair Use](https://copyrightalliance.org/faqs/what-is-fair-use/) in the United States.  Specifically, this is a research project to see if it is possible to replicate a video game interface as a website while getting a perfect score using [Google Lighthouse](https://developer.chrome.com/docs/lighthouse/overview/).  Code is available under the [MIT License](https://mit-license.org/), so feel free to use code samples to document other monster RPGs.
