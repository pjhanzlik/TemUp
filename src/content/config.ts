import { z, defineCollection, reference} from 'astro:content';

const species = defineCollection({
    type: 'data',
    schema: z.object({
        no: z.number().int().positive(),
        types: z.array(reference('types')).max(2).nonempty(),
        hp: z.number().int().positive(),
        sta: z.number().int().positive(),
        spd: z.number().int().positive(),
        atk: z.number().int().positive(),
        def: z.number().int().positive(),
        spatk: z.number().int().positive(),
        spdef: z.number().int().positive(),
        tempedia: z.string().trim(),
        traits: z.array(reference('traits')).length(2),
        genderRatio: z.number().int().nonnegative().optional(),
        catchRate: z.number().int().nonnegative(),
        experienceGroup: z.enum(['UltraLow', 'SuperLow', 'VeryLow', 'Low', 'Normal', 'High', 'VeryHigh', 'SuperHigh', 'UltraHigh']),
        radarCost: z.number().int().nonnegative(),
        lumaDrops: z.number().int().nonnegative(),
        tv: z.object({
            hp: z.number().int().positive(),
            sta: z.number().int().positive(),
            spd: z.number().int().positive(),
            atk: z.number().int().positive(),
            def: z.number().int().positive(),
            spatk: z.number().int().positive(),
            spdef: z.number().int().positive(),
        }).partial(),
        height: z.number().int().positive(),
        weight: z.number().int().positive(),
        evolution: z.record(reference('species'),
            z.object({
                levels: z.number().int().positive().max(99),
                trade: z.boolean(),
                gender: z.enum(['male', 'female']),
                trait: reference('traits'),
                shrine: z.enum(["Dabmis' Rest", "Altar of the Inner Flame", "Sons of Crystal", "Aisha's Hearth", "Digital Kami Shrine", "Chieftain's Barrow"]),
                tvs: z.number().int().positive().max(1000)
            }).partial()
        ).optional(),
        levelTechniques: z.record(reference('techniques'),z.number().positive().max(100)),
        courseTechniques: z.array(reference('techniques'))
    })
})

const techniques = defineCollection({
    type: 'data',
    schema: z.object({
        effect: z.string().trim(),
        inheritable: z.boolean(),
        course: z.number().int().nonnegative().lte(999).optional(),
        type: reference('types'),
        damage: z.object({
            amount: z.number().int().positive(),
            class: z.enum(['Physical', 'Special'])
        }).optional(),
        cost: z.number().int().positive(),
        synergy: z.object({
            type: reference('types'),
            damage: z.number().int().nonnegative().optional(),
            effect: z.string().trim().optional(),
            targeting: z.enum(['Self', 'Single Target', 'Single other Target', 'Single Team', 'Other Team or Ally', 'All', 'All Other Temtem']).optional(),
            priority: z.enum(['VeryLow', 'Low', 'Normal', 'High', 'VeryHigh', 'Extreme']).optional(),
            cost: z.number().int().positive().optional(),
            class: z.enum(['Physical', 'Special']).optional()
        }).optional(),
        hold: z.number().int().positive().optional(),
        priority: z.enum(['VeryLow', 'Low', 'Normal', 'High', 'VeryHigh', 'Extreme']),
        targeting: z.enum(['Self', 'Single Target', 'Single other Target', 'Single Team', 'Other Team or Ally', 'All', 'All Other Temtem'])
    })
})

const traits = defineCollection({
    type: 'data',
    schema: z.string().trim()
})

const types = defineCollection({
    type: 'data',
    schema: z.object({
        description: z.string().trim(),
        weaknesses: z.set(reference('types')).optional(),
        resistances: z.set(reference('types')).optional(),
        immunities: z.set(reference('conditions')).optional()
    })
})

const conditions = defineCollection({
    type: 'data',
    schema: z.string().trim()
})

const gear = defineCollection({
    type: 'data',
    schema: z.string().trim()
})

export const collections = {
    species,
    techniques,
    traits,
    types,
    conditions,
    gear
}
